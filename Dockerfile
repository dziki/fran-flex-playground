FROM php:7.1-apache

ENV COMPOSER_ALLOW_SUPERUSER 1

COPY env/docker/app/vhost.conf /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod rewrite \
    && apt-get update \
    && apt-get install -y \
        git \
        libfreetype6-dev \
        libmcrypt-dev \
        libpng-dev \
        libicu-dev \
    && apt-get clean
RUN docker-php-ext-install -j$(nproc) \
        intl \
        zip \
        pdo_mysql \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www/project

WORKDIR /var/www/project

RUN composer install --prefer-dist --no-dev --no-progress --no-suggest --classmap-authoritative --no-interaction \
    && usermod -u 1000 www-data \
    && chown -R www-data var
