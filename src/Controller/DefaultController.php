<?php
/**
 * Created by PhpStorm.
 * User: fran
 * Date: 21.11.17
 * Time: 22:57
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function index()
    {
        return new JsonResponse(['ok']);
    }
}