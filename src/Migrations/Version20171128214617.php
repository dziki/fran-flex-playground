<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128214617 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('
          CREATE TABLE product (
            id INT AUTO_INCREMENT NOT NULL, 
            name VARCHAR(256) DEFAULT NULL, 
            PRIMARY KEY(id)
        ) 
        DEFAULT CHARACTER SET utf8 
        COLLATE utf8_unicode_ci ENGINE = InnoDB;');

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DROP TABLE product;");
    }
}
